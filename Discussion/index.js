//JS ES6 Updates
// ECMAScript - the technology that is used to createe the languages such as JavaScript.

// Exponent Operator
	//pre-ES6
	const firstNum = Math.pow(8,2);
	console.log(firstNum);

	//ES6
	const secondNum = 8**2;
	console.log(secondNum);

// Template Literals - allows writing of strings without the use of concat operator
/*
create a name variable and store a name of a person
create a message, greeting the person and welcoming him/her in the programming field
log in the console message
*/

//pre-es6
let name = "John";

let message = "Hello " + name + " ! Welcome to the world of programming!"
console.log(message);

//ES6
message = `${name}, welcome to the world of programming!`
console.log(message);

//Multiline
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the answer of ${secondNum}
`
console.log(anotherMessage);

//Computation inside the template literals
const interestRate = 0.10;
const principal = 1000;
console.log(`The interest on your savings is ${principal*interestRate}.`);
//Array Restructuring - allows unpacking elements in arrays into distinct variables; allows naming of array elements with variable instead of using index numbers; helps with the code readability and coding efficiency
/*
	SYNTAX
	let/const = [variableA, variableB ...] = arrayName
*/
//pre-es6
fullName = ["Juan", "Dela", "Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

const [firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}`);

//Object Destructuring
let woman = {
	givenName: "Maria",
	maidenName: "Maharlika",
	familyName: "Clara"
};
console.log(woman.givenName)
console.log(woman.maidenName)
console.log(woman.familyName)
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`);

const {givenName, maidenName, familyName} = woman;
console.log(givenName)
console.log(maidenName)
console.log(familyName)
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

/*
Arrow Function
	
	compact alternative syntax to tradition functionsl useful for code snippets where creating functions will be reused in any other parts of the code; Don't repeat yourself - there is no need to create a function that will not be used in other parts/portions of the code
	
	pre-cs6

	Parts:
		declaration
		functionName
		parameters
		statements

*/



function printfullName(firstName, middleInitial, lastName){
	console.log(firstName);
	console.log(middleInitial);
	console.log(lastName);
}

printfullName("Gab", "P", "Choa");

//es6

const printFName = (fname, mname, lname) =>{
	console.log(fname, mname, lname);
}

printFName("Will", "D.", "Smith");

/*
	use forEach method to log each student in the console
*/

const students = ['John', 'Jane', 'Joe'];

students.forEach(
	function(element){
		console.log(element);
	});

//es6
//	if you have 2 or more paramets, enclose them inside a pair of parenthesis
students.forEach(x=>console.log(x));

/*

*/
//pre-es6
function addNumbers(x,y){
	return x + y;
};
let total = addNumbers(1,2);
console.log(total);

const adds = (x,y) => x+y;
//the code above actually runs as const adds = (x,y) => return x+y;
let totals = adds(4,6);
console.log(totals);

// (Arrow Function) Default Function Argument Value

const greet = (name = "User") =>{
	return `Good Morning, ${name}`;
};

console.log(greet());
// once the function has specified parameter value
console.log(`Result of specified value for parameter ${greet("John")}`);

/*
	create a car object using the object method
	car field: name, brand, year;
*/

/*function car(name, brand, year){
	this.name = name,
	this.brand = brand,
	this.year = year
}

const car1 = new car("Honda", "Vios", 2020);
console.log(car1);*/

//class constructor
// class keyword declares the creation of a "car" object

class car{
	//constructor keyword - a special method of creating/initializing an object for the "car" class
	constructor (brand, name, year){
		// this - sets the properties that refers to the class "car"
		this.brand = brand,
		this.name = name,
		this.year = year
	}
};

const car1 = new car("Ford", "Ranger", 2021);
console.log(car1);

const car2 = new car();
console.log(car2);
car2.brand = "Toyota";
car2.name = "Fortuner";
car2.year = 2021;
console.log(car2);

/*
create a variable that stores a number
create an if else statement using ternary operator
condition - if the number is <=0, return true, if it is >0 return false*/

let num = -10;
(num <= 0) ? console.log(true) : console.log(false)
/*
if(num <= 0){
	console.log(true);
}
else if(num > 0){
	console.log(false);
}*/
