var getCube = 2**3;
console.log(`The cube of 2 is ${getCube}.`);


let address = [258, "Washington Ave NW", "California", 90011]
const [houseNo, street, state, postal] = address;
message = `I live at ${houseNo} ${street}, ${state} ${postal}.`
console.log(message);

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	length: [20, 3]
};

const {name, type, weight, length} = animal;
message = `${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length[0]} ft ${length[1]} in.`
console.log(message);

let nums = [1,2,3,4,5];
nums.forEach(x=>console.log(x));

const reduceNumber = nums.reduce((total,amount) => total + amount);
console.log(reduceNumber);

class dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const dog1 = new dog("Frankie", 5, "Miniature Dachshund");
console.log(dog1);